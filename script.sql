USE [Agencia]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 11/4/2019 6:00:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[ID_Usuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Apellido] [varchar](100) NOT NULL,
	[Email] [varchar](100) NULL,
	[username] [varchar](50) NOT NULL,
	[pasword] [varchar](50) NOT NULL,
	[activo] [bit] NOT NULL,
	[eliminado] [bit] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Viaje_V]    Script Date: 11/4/2019 6:00:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Viaje_V](
	[ID_Viaje_V] [int] IDENTITY(1,1) NOT NULL,
	[ID_Viajes] [int] NOT NULL,
	[ID_Viajero] [int] NOT NULL,
 CONSTRAINT [PK_Viaje_V] PRIMARY KEY CLUSTERED 
(
	[ID_Viaje_V] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Viajero]    Script Date: 11/4/2019 6:00:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Viajero](
	[ID_Viajero] [int] IDENTITY(1,1) NOT NULL,
	[CI] [varchar](50) NOT NULL,
	[Primer_Nombre] [varchar](50) NOT NULL,
	[Segundo_Nombre] [varchar](50) NULL,
	[Primer_Apellido] [varchar](50) NOT NULL,
	[Segundo_Apellido] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Pasaporte] [varchar](50) NULL,
	[Direccion] [varchar](1000) NOT NULL,
	[Telefono1] [varchar](50) NOT NULL,
	[Telefono2] [varchar](50) NULL,
 CONSTRAINT [PK_Viajero] PRIMARY KEY CLUSTERED 
(
	[ID_Viajero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Viajes]    Script Date: 11/4/2019 6:00:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Viajes](
	[ID_Viajes] [int] IDENTITY(1,1) NOT NULL,
	[codigo_viajes] [varchar](50) NOT NULL,
	[numero_plazas] [int] NOT NULL,
	[destino] [varchar](100) NOT NULL,
	[origen] [varchar](100) NOT NULL,
	[precio] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Viajes] PRIMARY KEY CLUSTERED 
(
	[ID_Viajes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Viaje_V]  WITH CHECK ADD  CONSTRAINT [FK_Viaje_V_Viajero] FOREIGN KEY([ID_Viajero])
REFERENCES [dbo].[Viajero] ([ID_Viajero])
GO
ALTER TABLE [dbo].[Viaje_V] CHECK CONSTRAINT [FK_Viaje_V_Viajero]
GO
ALTER TABLE [dbo].[Viaje_V]  WITH CHECK ADD  CONSTRAINT [FK_Viaje_V_Viajes] FOREIGN KEY([ID_Viajes])
REFERENCES [dbo].[Viajes] ([ID_Viajes])
GO
ALTER TABLE [dbo].[Viaje_V] CHECK CONSTRAINT [FK_Viaje_V_Viajes]
GO
